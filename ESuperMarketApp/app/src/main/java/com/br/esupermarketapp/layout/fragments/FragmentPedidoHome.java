package com.br.esupermarketapp.layout.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.br.esupermarketapp.R;
import com.br.esupermarketapp.model.cadastro.Bairro;
import com.br.esupermarketapp.model.cadastro.Cep;
import com.br.esupermarketapp.model.cadastro.Cliente;
import com.br.esupermarketapp.model.cadastro.ClienteEndereco;
import com.br.esupermarketapp.model.cadastro.FormaPagamento;
import com.br.esupermarketapp.model.cadastro.Municipio;
import com.br.esupermarketapp.model.faturamento.Pedido;
import com.br.esupermarketapp.util.TaskType;
import com.br.esupermarketapp.util.Task;
import com.br.esupermarketapp.util.Util;
import com.br.esupermarketapp.webservice.IntegrationCallBack;
import com.br.esupermarketapp.webservice.IntegrationTaskService;
import com.br.esupermarketapp.webservice.PostExecute;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FragmentPedidoHome extends Fragment implements IntegrationCallBack, View.OnClickListener {

    public static final String TITLE = "Home";
    private View root;

    public Spinner  spinnerFormaPagamento;
    private EditText etNomeCliente;
    private EditText etCpf;
    private EditText etRua;
    private EditText etEnderecoNumero;
    private EditText etBairro;
    private EditText etMunicipio;
    private EditText etUf;
    private EditText etCep;
    private EditText etEmail;
    private EditText etTelefone;
    private EditText etComplemento;
    private TextView txDataPedido;

    private EditText etObs;

    private TableRow tableSpinnerProgress;
    private TableRow tableSpinnerFormaPagamento;
    private ProgressBar progressBarSpinner;
    private Button btFinalizar;

    private Pedido pedido;
    private List<FormaPagamento> formasFormaPagamentos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root == null){
            root  = inflater.inflate(R.layout.fragment_pedido_home, container, false);
            //CAMPOS CLIENTE
            etNomeCliente    = (EditText) root.findViewById(R.id.pedido_cliente_nome);
            etCpf            = (EditText) root.findViewById(R.id.pedido_cliente_cpf);
            etRua            = (EditText) root.findViewById(R.id.pedido_cliente_rua);
            etEnderecoNumero = (EditText) root.findViewById(R.id.pedido_cliente_endereco_numero);
            etBairro         = (EditText) root.findViewById(R.id.pedido_cliente_bairro);
            etMunicipio      = (EditText) root.findViewById(R.id.pedido_cliente_municipio);
            etUf             = (EditText) root.findViewById(R.id.pedido_cliente_uf);
            etCep            = (EditText) root.findViewById(R.id.pedido_cliente_cep);
            etEmail          = (EditText) root.findViewById(R.id.pedido_cliente_email);
            etTelefone       = (EditText) root.findViewById(R.id.pedido_cliente_telefone);
            etComplemento    = (EditText) root.findViewById(R.id.pedido_cliente_complemento);

            //pedido
            etObs = (EditText) root.findViewById(R.id.pedido_observacao);
            txDataPedido = (TextView) root.findViewById(R.id.pedido_data_pedido);
            tableSpinnerProgress       = (TableRow) root.findViewById(R.id.tableRowSpinnerProgress);
            tableSpinnerFormaPagamento = (TableRow) root.findViewById(R.id.tableRowSpinnerPagamento);
            progressBarSpinner         = (ProgressBar) root.findViewById(R.id.pedido_forma_pagamento_spinner_progressbar);

            progressBarSpinner.setIndeterminate(true);
            spinnerFormaPagamento = (Spinner) root.findViewById(R.id.pedido_forma_pagamento_spinner);
            txDataPedido.setText(Util.formatDate(new Date()));

            Task task = new Task(TaskType.LISTA_FORMA_PAGAMENTO);
            task.addParameter(pedido.getSupermercado());
            executeTask(task);

            fillPedidoView();
        }
        return root;
    }

    @Override
    public void beforeUpdateAdapter() {
        tableSpinnerProgress.setVisibility(View.VISIBLE);
        tableSpinnerFormaPagamento.setVisibility(View.GONE);
    }

    @Override
    public void afterUpdateAdapter() {
        tableSpinnerProgress.setVisibility(View.GONE);
        tableSpinnerFormaPagamento.setVisibility(View.VISIBLE);
    }

    @Override
    public void executeTask(Task task) {
        if (task.getType().equals(TaskType.LISTA_FORMA_PAGAMENTO)){
            beforeUpdateAdapter();
        }
        new IntegrationTaskService(this).execute(task);
    }

    @Override
    public void publishCallBack(PostExecute postExecute) {
        if (postExecute.getTaskExecuted().getType().equals(TaskType.LISTA_FORMA_PAGAMENTO)){
            formasFormaPagamentos = (List<FormaPagamento>) postExecute.getObjectReturnedFromIntegrationTask();
            updateAdapter(formasFormaPagamentos);
        }
        afterUpdateAdapter();
    }

    private void updateAdapter(List<FormaPagamento> formaPagamentos){
        List<String> valoresForPagtos = new ArrayList<>();
        for (FormaPagamento forPagto : formaPagamentos) {
            valoresForPagtos.add(forPagto.getDescricao());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, valoresForPagtos);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFormaPagamento.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        if (view == btFinalizar){

        }
    }

    public static FragmentPedidoHome newInstace(Pedido pedido){
        FragmentPedidoHome fragment = new FragmentPedidoHome();
        fragment.pedido = pedido;
        return fragment;
    }

    private Cliente fillCliente(){
        Cliente cliente = new Cliente();
        cliente.setId(null);
        cliente.setNome(etNomeCliente.getText().toString());
        cliente.setEmail(etEmail.getText().toString());
        cliente.setCpfCnpj(etCpf.getText().toString());
        cliente.setIndicadorUso(Cliente.INDICADOR_USO_ATIVO);
        cliente.setTipo(Cliente.CLIENTE_TIPO_FISICA);
        cliente.setTelefone(etTelefone.getText().toString());
        cliente.setRg(null);
        cliente.setSupermercado(pedido.getSupermercado());


        Municipio municipio = new Municipio();
        municipio.setId(null);
        municipio.setNome(etMunicipio.getText().toString());
        municipio.setUf(etUf.getText().toString());

        Cep cep = new Cep();
        cep.setId(null);
        cep.setCodigo(Integer.parseInt(Util.removerCaracteres(etCep.getText().toString())));

        Bairro bairro = new Bairro();
        bairro.setId(null);
        bairro.setNome(etBairro.getText().toString());

        ClienteEndereco clienteEndereco = new ClienteEndereco();
        clienteEndereco.setCliente(cliente);
        clienteEndereco.setMunicipio(municipio);
        clienteEndereco.setCep(cep);
        clienteEndereco.setBairro(bairro);
        clienteEndereco.setNumero(etEnderecoNumero.getText().toString());
        clienteEndereco.setRua(etRua.getText().toString());

        cliente.setEndereco(clienteEndereco);
        return cliente;
    }

    public Pedido fillPedido(){
        pedido.setCliente(fillCliente());
        pedido.setFormaPagamento(formasFormaPagamentos.get(spinnerFormaPagamento.getSelectedItemPosition()));
        pedido.setData(new Date());
        pedido.setSituacao(Pedido.STATUS_PEDIDO_PENDENTE);
        pedido.setObs(etObs.getText().toString());
        return pedido;
    }

    private void fillPedidoView(){
        etNomeCliente.setText("EDUARDO YUCHO");
        etTelefone.setText("48 9 99999999");
        etBairro.setText("TESTE");
        etCep.setText("88750-000");
        etCpf.setText("999.999.999.99");
        etEmail.setText("EDUARDO.YUCHO@outlook.com");
        etMunicipio.setText("teste");
        etObs.setText("TESTE");
        etRua.setText("TESTE");
        etUf.setText("SC");
        etEnderecoNumero.setText("S/N");
        etComplemento.setText("SEM REFERÊNCIA");
    }

    public FormaPagamento getFormaPagamento(){
        return formasFormaPagamentos.get(spinnerFormaPagamento.getSelectedItemPosition());
    }
}


