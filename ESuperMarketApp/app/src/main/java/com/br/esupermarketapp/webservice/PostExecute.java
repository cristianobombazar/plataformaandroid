package com.br.esupermarketapp.webservice;


import com.br.esupermarketapp.util.Task;
import com.br.esupermarketapp.util.TaskType;

public class PostExecute {

    private Object objectReturnedFromIntegrationTask;
    private Task taskExecuted;

    public PostExecute(Object objectReturnedFromIntegrationTask, Task taskExecuted){
        this.objectReturnedFromIntegrationTask = objectReturnedFromIntegrationTask;
        this.taskExecuted = taskExecuted;
    }

    public void setObjectReturnedFromIntegrationTask(Object objectReturnedFromIntegrationTask) {
        this.objectReturnedFromIntegrationTask = objectReturnedFromIntegrationTask;
    }

    public Object getObjectReturnedFromIntegrationTask() {
        return this.objectReturnedFromIntegrationTask;
    }

    public void setTaskExecuted(Task taskExecuted) {
        this.taskExecuted = taskExecuted;
    }

    public Task getTaskExecuted() {
        return taskExecuted;
    }
}
