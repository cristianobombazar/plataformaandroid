package com.br.esupermarketapp.layout.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.br.esupermarketapp.R;
import com.br.esupermarketapp.model.cadastro.Produto;
import com.br.esupermarketapp.model.faturamento.Pedido;
import com.br.esupermarketapp.model.faturamento.PedidoItem;
import com.br.esupermarketapp.util.Util;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;


public class AdapterPedidoItem extends RecyclerView.Adapter<AdapterPedidoItem.PedidoItemViewHolder> {

    private Context context;
    private Pedido  pedido;
    private List<PedidoItem> produtos;
    private static ItemClickListener itemClickListener;

    public AdapterPedidoItem(Pedido pedido, Context context, ItemClickListener itemClickListener){
        this.pedido            = pedido;
        this.produtos          = pedido.getItens();
        this.context           = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public PedidoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_pedido_item, parent, false);
        return new PedidoItemViewHolder(view, new MyTextWatcher());
    }

    @Override
    public void onBindViewHolder(final PedidoItemViewHolder holder, final int position) {
        PedidoItem pedidoItem = produtos.get(position);
        Produto produto = pedidoItem.getProduto();

        holder.pedidoItem = pedidoItem;
        holder.header.setText("COD: "+produto.getId()+". Descrição: "+produto.getDescricao());
        String descricaoOferta = "Oferta válida somente entre os dias "+Util.formatDate(pedido.getOferta().getDataInicio())+" e "+Util.formatDate(pedido.getOferta().getDataFim());
        holder.content.setText("MARCA: "+produto.getMarca()+".\n"+descricaoOferta);
        holder.etQuantidade.setText(Util.formatarValor(BigDecimal.ONE));
    }

    @Override
    public int getItemCount() {
        return produtos.size();
    }

    public List<PedidoItem> getData(){
        return Collections.unmodifiableList(produtos);
    }

    public void removerProduto(PedidoItem pedidoItem){
        produtos.remove(pedidoItem);
    }

    private void setItens(List<PedidoItem> itens){
        this.produtos = itens;
    }

    public static class PedidoItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        private TextView      header;
        private TextView      content;
        private EditText      etQuantidade;
        private PedidoItem pedidoItem;

        public PedidoItemViewHolder(View item, MyTextWatcher myWatcher){
            super(item);
            this.header          = (TextView) item.findViewById(R.id.pedido_item_produto_header);
            this.content         = (TextView) item.findViewById(R.id.pedido_item_produto_content);
            this.etQuantidade    = (EditText) item.findViewById(R.id.pedido_item_quantidade);
            this.etQuantidade.addTextChangedListener(myWatcher);
            item.setOnClickListener(this);
            item.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick(pedidoItem);
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.setOnLongClickListener(pedidoItem);
            return true;
        }

    }

    public interface ItemClickListener{
        void onItemClick(PedidoItem pedidoItem);
        void updateAdapter();
        void setOnLongClickListener(PedidoItem pedidoItem);
    }

    private class MyTextWatcher implements TextWatcher {

        private int positionToUpdate;

        public void updatePosition(int positionToUpdate){
            this.positionToUpdate = positionToUpdate;
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            if (charSequence != null && charSequence.length() > 0){
                double quantidade = Double.parseDouble(charSequence.toString().replace(".", "").replace(",", "."));
                produtos.get(positionToUpdate).setQuantidade(new BigDecimal(quantidade));
            }
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }



}
