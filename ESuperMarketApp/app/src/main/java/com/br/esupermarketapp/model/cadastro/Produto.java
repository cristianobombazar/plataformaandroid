package com.br.esupermarketapp.model.cadastro;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class Produto implements Serializable {
	
	private Integer id;
	private String descricao;
	private BigDecimal valor;
	private BigDecimal quantidade;
	private String marca;
	private String qrCode;
	@Expose(serialize = false, deserialize = false)
	private Supermercado supermercado;
	private String obs;
	private Long generatedValueQrCodeEncoder;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDescricao() {
		return descricao;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public BigDecimal getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getQrCode() {
		return qrCode;
	}
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	public Supermercado getSupermercado() {
		return supermercado;
	}
	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}
	
	public void setObs(String obs) {
		this.obs = obs;
	}
	public String getObs() {
		return obs;
	}
	
	public void setGeneratedValueQrCodeEncoder(Long generatedValueQrCodeEncoder) {
		this.generatedValueQrCodeEncoder = generatedValueQrCodeEncoder;
	}
	
	public Long getGeneratedValueQrCodeEncoder() {
		return generatedValueQrCodeEncoder;
	}
}
