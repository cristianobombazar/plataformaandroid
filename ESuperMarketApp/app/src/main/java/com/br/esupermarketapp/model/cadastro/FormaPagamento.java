package com.br.esupermarketapp.model.cadastro;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class FormaPagamento {
	
	public static final Integer INDICADOR_USO_ATIVO = 1;
	public static final Integer INDICADOR_USO_INATIVO = 2;

    public static final Integer INDICADOR_CARTAO_CREDITO_SIM = 1;
    public static final Integer INDICADOR_CARTAO_CREDITO_NAO = 2;
	
	private Integer id;
	private String descricao;
	private Integer indicadorUso;
    private Integer indicadorCartaoCredito;
	private Supermercado supermercado;
	
	public FormaPagamento() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getIndicadorUso() {
		return indicadorUso;
	}

	public void setIndicadorUso(Integer indicadorUso) {
		this.indicadorUso = indicadorUso;
	}
	
	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}
	
	public Supermercado getSupermercado() {
		return supermercado;
	}

    public void setIndicadorCartaoCredito(Integer indicadorCartaoCredito) {
        this.indicadorCartaoCredito = indicadorCartaoCredito;
    }

    public Integer getIndicadorCartaoCredito() {
        return indicadorCartaoCredito;
    }

    @Override
	public String toString() {
		return this.id +" - "+this.descricao;
	}
}
