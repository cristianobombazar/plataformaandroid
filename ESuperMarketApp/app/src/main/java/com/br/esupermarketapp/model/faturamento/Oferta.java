package com.br.esupermarketapp.model.faturamento;

import android.os.Parcel;
import android.os.Parcelable;

import com.br.esupermarketapp.model.cadastro.Supermercado;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class Oferta implements Serializable, Cloneable {
	
	private Integer id;
	private String descricao;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date dataCadastro;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date dataInicio;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date dataFim;
	private Supermercado supermercado;
	private String obs;
	@JsonManagedReference
	private List<OfertaProduto> produtos = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public Supermercado getSupermercado() {
		return supermercado;
	}
	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	
	public List<OfertaProduto> getProdutos(){
		return produtos;
	}
	
	public void adicionaOfertaProduto(OfertaProduto ofertaProduto) {
		produtos.add(ofertaProduto);
	}
	
	public void removerOfertaProduto(OfertaProduto ofertaProduto) {
		produtos.remove(ofertaProduto);
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
	    return super.clone();
	}
}
