package com.br.esupermarketapp.layout.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.br.esupermarketapp.R;
import com.br.esupermarketapp.model.cadastro.Produto;
import com.br.esupermarketapp.model.faturamento.Oferta;
import com.br.esupermarketapp.model.faturamento.OfertaProduto;

import java.util.Collections;
import java.util.List;


public class AdapterProduto extends RecyclerView.Adapter<AdapterProduto.ProdutoViewHolder> {

    private Oferta oferta;
    private List<OfertaProduto> produtos;
    private static ItemClickListener itemClickListener;

    public AdapterProduto(Oferta oferta, ItemClickListener itemClickListener){
        this.oferta            = oferta;
        this.produtos          = oferta.getProdutos();
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ProdutoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_produto, parent, false);
        return new ProdutoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProdutoViewHolder holder, final int position) {
        final OfertaProduto ofertaProduto = produtos.get(position);

        Produto produto = ofertaProduto.getProduto();

        holder.produto = ofertaProduto;
        holder.header.setText("COD: "+produto.getId()+". Descrição: "+produto.getDescricao());
        holder.content.setText("MARCA: "+produto.getMarca()+". Qtd Disponível: "+ofertaProduto.getQuantidadeDisponivel());
        holder.checkBox.setChecked(produtos.get(position).isSelected());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                produtos.get(position).setSelected(isChecked);
                itemClickListener.updateAdapter();
            }
        });
        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = produtos.indexOf(ofertaProduto);
                produtos.get(position).setSelected(!produtos.get(position).isSelected());
                holder.checkBox.setChecked(produtos.get(position).isSelected());
                itemClickListener.updateAdapter();
            }
        });
        holder.header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = produtos.indexOf(ofertaProduto);
                produtos.get(position).setSelected(!produtos.get(position).isSelected());
                holder.checkBox.setChecked(produtos.get(position).isSelected());
                itemClickListener.updateAdapter();
            }
        });
    }


    @Override
    public int getItemCount() {
        return produtos.size();
    }

    public List<OfertaProduto> getData(){
        return Collections.unmodifiableList(produtos);
    }

    public Oferta getDataSuper(){
        return oferta;
    }

    public void addSupermercado(OfertaProduto ofertaProduto){
        this.produtos.add(0, ofertaProduto);
    }

    public void removeSupermercado(OfertaProduto ofertaProduto){
        this.produtos.remove(ofertaProduto);
    }

    public static class ProdutoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private CheckBox      checkBox;
        private TextView      header;
        private TextView      content;
        private OfertaProduto produto;

        public ProdutoViewHolder(View item){
            super(item);
            this.header    = (TextView) item.findViewById(R.id.item_produto_header);
            this.content   = (TextView) item.findViewById(R.id.item_produto_content);
            this.checkBox  = (CheckBox) item.findViewById(R.id.item_produto_check);
            item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick(produto);
        }
    }

    public interface ItemClickListener{
        void onItemClick(OfertaProduto ofertaProduto);
        void updateAdapter();
    }

}
