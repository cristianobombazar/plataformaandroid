package com.br.esupermarketapp.model.seguranca;

import com.br.esupermarketapp.model.cadastro.Supermercado;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Set;


@SuppressWarnings("serial")
public class Usuario implements Serializable {

	private Integer id;
	private String nome;
	private String email;
	@JsonProperty
	private String login;
	@JsonProperty
	private String senha;
	private Date ultimaAlteracao;
	private Supermercado supermercado;

	private Set<UsuarioAutoridade> autoridades;

	public Usuario(String login, String senha) {
		this.login = login;
		this.senha = senha;
	}

	public Collection<UsuarioAutoridade> getAuthorities() {
		return autoridades;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Date getUltimaAlteracao() {
		return ultimaAlteracao;
	}

	public void setUltimaAlteracao(Date ultimaAlteracao) {
		this.ultimaAlteracao = ultimaAlteracao;
	}

	public Set<UsuarioAutoridade> getAutoridades() {
		return autoridades;
	}

	public void setAutoridades(Set<UsuarioAutoridade> autoridades) {
		this.autoridades = autoridades;
	}
	
	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}
	
	public Supermercado getSupermercado() {
		return supermercado;
	}

}
