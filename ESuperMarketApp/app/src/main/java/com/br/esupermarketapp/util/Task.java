package com.br.esupermarketapp.util;

import java.util.ArrayList;
import java.util.List;


public class Task {

    private List<Object> parameters = new ArrayList<>();
    private TaskType type;

    public Task(){
    }
    public Task(TaskType type){
        this.type       = type;
    }
    public Task(List<Object> parameters, TaskType type){
        this.parameters = parameters;
        this.type       = type;
    }
    public void addParameter(Object parameter){
        this.parameters.add(parameter);
    }

    public void removeParameter(Object parameter){
        this.parameters.remove(parameter);
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public TaskType getType() {
        return type;
    }

    public List<Object> getParameters() {
        return parameters;
    }
}
