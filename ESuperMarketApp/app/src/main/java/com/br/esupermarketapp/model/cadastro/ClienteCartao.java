package com.br.esupermarketapp.model.cadastro;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClienteCartao implements Serializable {
	
	public static final Integer CARTAO_TIPO_DEBITO = 1;
	public static final Integer CARTAO_TIPO_CREDITO = 2;
	public static final Integer CARTAO_TIPO_ALIMENTACAO = 3;
	public static final Integer CARTAO_TIPO_OUTRO = 4;
	
	public static final Integer CARTAO_BANDEIRA_VISA = 1;
	public static final Integer CARTAO_BANDEIRA_MASTERCARD = 2;
	public static final Integer CARTAO_BANDEIRA_ELO = 3;
	public static final Integer CARTAO_BANDEIRA_AMERICAN_EXPRESS = 4;
	public static final Integer CARTAO_BANDEIRA_DISCOVERY_NETWORK = 5;
	public static final Integer CARTAO_BANDEIRA_DISCOVERY_OUTRO = 6;

	private Integer id;
	private Integer tipo;
	private Integer bandeira;
	@JsonBackReference
	private Cliente cliente;
	private String numeroCartao;
	private Date dataExpiracao;
	private Integer codigoSeguranca;
	
	private String nomeImpresso;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getNumeroCartao() {
		return numeroCartao;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}

	public Date getDataExpiracao() {
		return dataExpiracao;
	}

	public void setDataExpiracao(Date dataExpiracao) {
		this.dataExpiracao = dataExpiracao;
	}

	public Integer getCodigoSeguranca() {
		return codigoSeguranca;
	}

	public void setCodigoSeguranca(Integer codigoSeguranca) {
		this.codigoSeguranca = codigoSeguranca;
	}

	public String getNomeImpresso() {
		return nomeImpresso;
	}

	public void setNomeImpresso(String nomeImpresso) {
		this.nomeImpresso = nomeImpresso;
	}
	
	public void setBandeira(Integer bandeira) {
		this.bandeira = bandeira;
	}
	
	public Integer getBandeira() {
		return bandeira;
	}
	
}
