package com.br.esupermarketapp.webservice;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;
import java.io.IOException;


public class AuthenticationHeaderInterceptor implements ClientHttpRequestInterceptor {
    private final String auth;

    public AuthenticationHeaderInterceptor(String auth) {
        this.auth = auth;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        HttpRequest wrapper = new HttpRequestWrapper(request);
        wrapper.getHeaders().set("Authorization", auth);
        return execution.execute(wrapper, body);
    }
}
