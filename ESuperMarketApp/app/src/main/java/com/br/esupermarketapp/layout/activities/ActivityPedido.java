package com.br.esupermarketapp.layout.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.br.esupermarketapp.R;
import com.br.esupermarketapp.layout.adapters.AdapterPedido;
import com.br.esupermarketapp.layout.fragments.FragmentPedidoCartao;
import com.br.esupermarketapp.layout.fragments.FragmentPedidoHome;
import com.br.esupermarketapp.layout.fragments.FragmentPedidoItem;
import com.br.esupermarketapp.model.cadastro.ClienteCartao;
import com.br.esupermarketapp.model.cadastro.FormaPagamento;
import com.br.esupermarketapp.model.faturamento.Oferta;
import com.br.esupermarketapp.model.faturamento.OfertaProduto;
import com.br.esupermarketapp.model.faturamento.Pedido;
import com.br.esupermarketapp.model.faturamento.PedidoItem;
import com.br.esupermarketapp.util.TaskType;
import com.br.esupermarketapp.util.Task;
import com.br.esupermarketapp.webservice.IntegrationCallBack;
import com.br.esupermarketapp.webservice.IntegrationTaskService;
import com.br.esupermarketapp.webservice.PostExecute;

import java.math.BigDecimal;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;

public class ActivityPedido extends AppCompatActivity implements View.OnClickListener, IntegrationCallBack {

    private ViewPager              viewPager      = null;
    private FragmentPedidoItem     fragmentItem   = null;
    private FragmentPedidoHome     fragmentHome   = null;
    private FragmentPedidoCartao   fragmentCartao = null;
    private TabLayout              tabLyout       = null;
    private Toolbar                toolbar        = null;
    private CircularProgressButton btFinalizar    = null;
    private ProgressBar            progressBar    = null;

    public static final String EXTRA_BUNDLE       = "ARGUMENTS_PEDIDO";
    public static final String EXTRA_SUPERMERCADO = "SUPERMERCADO";
    public static final String EXTRA_ITENS        = "PEDIDO_ITENS";


    private Pedido pedido = new Pedido();

    public ActivityPedido(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);

        Oferta oferta = (Oferta) getIntent().getExtras().getSerializable(ActivityPedido.EXTRA_ITENS);
        viewPager     = (ViewPager) findViewById(R.id.pedido_view_pager);
        tabLyout      = (TabLayout) findViewById(R.id.pedido_tabs);
        toolbar       = (Toolbar) findViewById(R.id.pedido_toolbar);
        btFinalizar   = (CircularProgressButton) findViewById(R.id.pedido_botao_finalizar);
        progressBar   = (ProgressBar) findViewById(R.id.pedido_finalizar_progressbar);
        toolbar.setTitle("Detalhe do Pedido");
        btFinalizar.setOnClickListener(this);
        btFinalizar.setPadding(40,0,40,0);
        setSupportActionBar(toolbar);
        fillPedido(oferta);

        fragmentHome    = FragmentPedidoHome.newInstace(pedido);
        fragmentItem    = FragmentPedidoItem.newInstace(pedido);
        fragmentCartao  = FragmentPedidoCartao.newInstace(pedido);

        final AdapterPedido adapter = new AdapterPedido(getSupportFragmentManager());
        adapter.addFragment(fragmentHome, FragmentPedidoHome.TITLE);
        adapter.addFragment(fragmentItem, FragmentPedidoItem.TITLE);
        adapter.addFragment(fragmentCartao, FragmentPedidoCartao.TITLE);
        viewPager.setAdapter(adapter);
        tabLyout.setupWithViewPager(viewPager);
    }

    @Override
    public void onClick(View view) {
        if (view == btFinalizar){
            pedido = fragmentHome.fillPedido();
            pedido.setItens(fragmentItem.fillPedidoItens());
            if (pedido.getFormaPagamento() != null && pedido.getFormaPagamento().getIndicadorCartaoCredito().equals(FormaPagamento.INDICADOR_CARTAO_CREDITO_SIM)){
                ClienteCartao clienteCartao = fragmentCartao.fillCartaoCliente(pedido.getCliente());

                if (clienteCartao != null){
                    pedido.getCliente().adicionarCartao(clienteCartao);
                }
            }
            Task task = new Task(TaskType.SALVAR_PEDIDO);
            task.addParameter(pedido);
            executeTask(task);
        }
    }

    private void fillPedido(Oferta oferta){
        pedido.setOferta(oferta);
        pedido.setSupermercado(oferta.getSupermercado());
        if (oferta.getProdutos() != null && !oferta.getProdutos().isEmpty()){
            for (OfertaProduto ofertaProduto: oferta.getProdutos()){
                PedidoItem pedidoItem = new PedidoItem();
                pedidoItem.setProduto(ofertaProduto.getProduto());
                pedidoItem.setQuantidade(BigDecimal.ONE);
                pedidoItem.setValor(pedidoItem.getQuantidade().multiply(ofertaProduto.getValor()));
                pedidoItem.setPedido(pedido);
                pedido.adicionaItem(pedidoItem);
            }
        }
    }

    @Override
    public void beforeUpdateAdapter() {
        progressBar.setVisibility(View.VISIBLE);
        btFinalizar.setVisibility(View.GONE);
    }
    @Override
    public void afterUpdateAdapter() {
        progressBar.setVisibility(View.GONE);
        btFinalizar.setVisibility(View.VISIBLE);
    }
    @Override
    public void publishCallBack(PostExecute postExecute) {
        if (postExecute.getTaskExecuted().getType().equals(TaskType.SALVAR_PEDIDO)){
            if (postExecute.getObjectReturnedFromIntegrationTask() instanceof Boolean){
                boolean resultOk = (boolean) postExecute.getObjectReturnedFromIntegrationTask();
                if (resultOk){
                    btFinalizar.setText("Pedido finalizado com sucesso!");
                    btFinalizar.setBackground(getResources().getDrawable(R.drawable.ic_done_white_48dp));
                    Toast.makeText(this, "Pedido enviado ao supermercado. Aguarte contato por telefone!", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(this, ActivityMain.class));
                }else{
                    btFinalizar.setBackgroundColor(Color.RED);
                    btFinalizar.setText("Erro ao finalizar pedido!");
                    Toast.makeText(this, "Erro ao enviar pedido, tente novamente mais tarde!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        afterUpdateAdapter();
    }

    @Override
    public void executeTask(Task task) {
        beforeUpdateAdapter();
        new IntegrationTaskService(this).execute(task);
    }
}
