package com.br.esupermarketapp.model.faturamento;

import android.os.Parcel;
import android.os.Parcelable;

import com.br.esupermarketapp.model.cadastro.Produto;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class OfertaProduto implements Serializable {

	private Integer id;
	@JsonBackReference
    @Expose(serialize = false, deserialize = false)
	private Oferta oferta;
	private Produto produto;
	private BigDecimal valor;
	private BigDecimal quantidade;
	private BigDecimal quantidadeDisponivel;
	private transient boolean selected;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Oferta getOferta() {
		return oferta;
	}
	public void setOferta(Oferta oferta) {
		this.oferta = oferta;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public BigDecimal getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}
	public void setQuantidadeDisponivel(BigDecimal quantidadeDisponivel) {
		this.quantidadeDisponivel = quantidadeDisponivel;
	}
	public BigDecimal getQuantidadeDisponivel() {
		return quantidadeDisponivel;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean isSelected() {
		return selected;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof OfertaProduto)) return false;

		OfertaProduto that = (OfertaProduto) o;

		return getId().equals(that.getId());

	}
	@Override
	public int hashCode() {
		return getId().hashCode();
	}

}
