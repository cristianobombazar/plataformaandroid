package com.br.esupermarketapp.layout.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.br.esupermarketapp.Global;
import com.br.esupermarketapp.R;
import com.br.esupermarketapp.layout.fragments.FragmentProduto;
import com.br.esupermarketapp.layout.fragments.FragmentSupermercado;
import com.br.esupermarketapp.model.faturamento.Oferta;
import com.br.esupermarketapp.model.faturamento.OfertaProduto;
import com.br.esupermarketapp.util.Task;
import com.br.esupermarketapp.util.TaskType;
import com.br.esupermarketapp.webservice.IntegrationCallBack;
import com.br.esupermarketapp.webservice.IntegrationTaskService;
import com.br.esupermarketapp.webservice.PostExecute;
import com.google.zxing.Result;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class ActivityMain extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, IntegrationCallBack {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private FloatingActionButton floatingActionButton;
    boolean backPressed = false;

    private AlertDialog alert;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.ic_camera_qrcode);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        setSupportActionBar(toolbar);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        floatingActionButton.setOnClickListener(this);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, new FragmentSupermercado());
        transaction.commit();
        Global.setPreviusFragment(new FragmentSupermercado());
        setTitleActionBar("Supermercados");
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (Global.previusFragment != null){
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, Global.previusFragment);
                transaction.commit();
            }else{
                if (backPressed) {
                    super.onBackPressed();
                } else {
                    Toast.makeText(this, "Toque voltar novamente para sair", Toast.LENGTH_SHORT).show();
                    backPressed = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            backPressed = false;
                        }
                    }, 2000);
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (id == R.id.nav_supermercados){
            transaction.replace(R.id.container, new FragmentSupermercado());
        }
        transaction.commit();
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setTitleActionBar(String text){
        toolbar.setTitle(text);
    }

    @Override
    public void onClick(View view) {
        if (view == floatingActionButton){
            IntentIntegrator integrator = new IntentIntegrator(this);
            integrator.addExtra("SCAN_MODE", "ONE_D_MODE");
            integrator.setPrompt("Scan");
            integrator.addExtra("SCAN_FORMATS", "DATA_MATRIX,CODABAR,EAN_13,UPC_A,QR_CODE");
            integrator.initiateScan();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (intentResult != null && intentResult.getContents() != null){
            String[] resultQrCode = intentResult.getContents().split(";");
            final Long resultBarCode  = resultQrCode[0] != null ? Long.parseLong(resultQrCode[0]) : null;
            String descricao    = resultQrCode[1];
            String supermercado = resultQrCode[2];
            if (descricao != null && supermercado != null){
                final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Light);
                alertBuilder.setTitle("Encontramos um produto...");
                alertBuilder.setMessage("Encontramos o produto '"+descricao+"' do supermercado '"+supermercado+"', deseja ir para este produto? ");
                alertBuilder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Task task = new Task(TaskType.BUSCA_PRODUTO_QRCODE);
                        task.addParameter(resultBarCode);
                        executeTask(task);
                    }
                });
                alertBuilder.setNegativeButton("Não", null);
                alert = alertBuilder.create();
                alert.show();
            }
        }

    }

    @Override
    public void afterUpdateAdapter() {
    }

    @Override
    public void beforeUpdateAdapter() {
    }

    @Override
    public void publishCallBack(PostExecute postExecute) {
        if (postExecute != null && postExecute.getTaskExecuted().getType().equals(TaskType.BUSCA_PRODUTO_QRCODE)){
            if (postExecute.getObjectReturnedFromIntegrationTask() != null && postExecute.getObjectReturnedFromIntegrationTask() instanceof Oferta){
                Oferta oferta = (Oferta) postExecute.getObjectReturnedFromIntegrationTask();
                if (oferta != null){
                    Long qrCode = null;
                    if (postExecute.getTaskExecuted() != null && postExecute.getTaskExecuted().getParameters() != null && !postExecute.getTaskExecuted().getParameters().isEmpty()){
                        qrCode = (Long) postExecute.getTaskExecuted().getParameters().get(0);
                    }
                    for(OfertaProduto ofertaProduto: oferta.getProdutos()){
                        if (ofertaProduto.getProduto().getGeneratedValueQrCodeEncoder().equals(qrCode)){
                            ofertaProduto.setSelected(true);
                            break;
                        }
                    }
                    Bundle parameters = new Bundle();
                    parameters.putSerializable(FragmentProduto.EXTRA_SUPERMERCADO, oferta.getSupermercado());
                    parameters.putSerializable(FragmentProduto.EXTRA_OFERTA, oferta);
                    FragmentProduto fragmentProduto = new FragmentProduto();
                    fragmentProduto.setArguments(parameters);

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.container, fragmentProduto);
                    transaction.commit();
                    Global.setPreviusFragment(fragmentProduto);
                }else{
                    Snackbar.make(floatingActionButton, "Desculpa, não encontramos nenhum produto com essa especificação :(", Snackbar.LENGTH_INDEFINITE).setAction("Action", null).show();
                }
            }else{
                Snackbar.make(floatingActionButton, "Desculpa, não encontramos nenhum produto com essa especificação :(", Snackbar.LENGTH_INDEFINITE).setAction("Action", null).show();
            }
     }else{
            Snackbar.make(floatingActionButton, "Desculpa, não encontramos nenhum produto com essa especificação :(", Snackbar.LENGTH_INDEFINITE).setAction("Action", null).show();
        }
    }

    @Override
    public void executeTask(Task task) {
        new IntegrationTaskService(this).execute(task);
    }


}

