package com.br.esupermarketapp.layout.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;

import com.br.esupermarketapp.R;
import com.br.esupermarketapp.layout.activities.ActivityPedido;
import com.br.esupermarketapp.layout.activities.ActivityMain;
import com.br.esupermarketapp.layout.adapters.AdapterProduto;
import com.br.esupermarketapp.model.cadastro.Bairro;
import com.br.esupermarketapp.model.cadastro.Supermercado;
import com.br.esupermarketapp.model.faturamento.Oferta;
import com.br.esupermarketapp.model.faturamento.OfertaProduto;
import com.br.esupermarketapp.util.TaskType;
import com.br.esupermarketapp.util.Task;
import com.br.esupermarketapp.webservice.IntegrationCallBack;
import com.br.esupermarketapp.webservice.IntegrationTaskService;
import com.br.esupermarketapp.webservice.PostExecute;


public class FragmentProduto extends Fragment implements AdapterProduto.ItemClickListener, IntegrationCallBack {

    private View           root            = null;
    private ProgressBar    progress        = null;
    private RecyclerView   recyclerView    = null;
    private AdapterProduto adapter         = null;
    private Supermercado   supermercado    = null;
    private Menu           menu            = null;
    private Oferta         oferta          = null;

    public static final String EXTRA_SUPERMERCADO = "SUPERMERCADO";
    public static final String EXTRA_OFERTA       = "OFERTA";

    private Activity mActivity = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root == null){
            root = inflater.inflate(R.layout.fragment_produto, container, false);
            progress     = (ProgressBar)  root.findViewById(R.id.fragment_produtos_progress);
            recyclerView = (RecyclerView) root.findViewById(R.id.fragment_produtos_recyler);
            oferta          = (Oferta) getArguments().getSerializable(FragmentProduto.EXTRA_OFERTA);
            recyclerView.setHasFixedSize(true);
            progress.setIndeterminate(true);
            setHasOptionsMenu(true);
            if (oferta != null){
                supermercado = oferta.getSupermercado();
                beforeUpdateAdapter();
                updateAdapter(oferta);
                afterUpdateAdapter();
            }else{
                supermercado    = (Supermercado) getArguments().getSerializable(FragmentProduto.EXTRA_SUPERMERCADO);
                Task task = new Task(TaskType.LISTA_PRODUTOS);
                task.addParameter(supermercado);
                executeTask(task);
            }
            ((ActivityMain) getActivity()).setTitleActionBar("Produtos");
            mActivity = getActivity();
        }
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void publishCallBack(PostExecute postExecute) {
        if (postExecute.getTaskExecuted().getType().equals(TaskType.LISTA_PRODUTOS)){
            updateAdapter((Oferta) postExecute.getObjectReturnedFromIntegrationTask());
            afterUpdateAdapter();
        }
    }

    @Override
    public void executeTask(Task task) {
        beforeUpdateAdapter();
        new IntegrationTaskService(this).execute(task);
    }

    @Override
    public void beforeUpdateAdapter() {
        progress.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

    }

    @Override
    public void afterUpdateAdapter() {
        progress.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }


    private void updateAdapter(Oferta oferta){
        if (oferta != null && oferta.getProdutos() != null && !oferta.getProdutos().isEmpty()){
            adapter = new AdapterProduto(oferta, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));

            LayoutAnimationController layoutAnimationController = AnimationUtils.loadLayoutAnimation(mActivity.getApplicationContext(), R.anim.layout_animation_slide_right);
            recyclerView.setLayoutAnimation(layoutAnimationController);
            recyclerView.setAdapter(adapter);
            recyclerView.scheduleLayoutAnimation();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            mActivity = (Activity) context;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.fragment_produto, menu);
        if (oferta != null){
            updateAdapter();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_produto_carrinho){
            Oferta oferta = new Oferta();
            Oferta ofertaProdutos = adapter.getDataSuper();
            oferta.setId(ofertaProdutos.getId());
            oferta.setDataCadastro(ofertaProdutos.getDataCadastro());
            oferta.setDataInicio(ofertaProdutos.getDataInicio());
            oferta.setDataFim(ofertaProdutos.getDataFim());
            oferta.setDescricao(ofertaProdutos.getDescricao());
            oferta.setObs(ofertaProdutos.getObs());
            oferta.setSupermercado(ofertaProdutos.getSupermercado());
            for (OfertaProduto ofertaProduto: adapter.getData()){
                if (ofertaProduto.isSelected()){
                    oferta.adicionaOfertaProduto(ofertaProduto);
                }
            }
            Intent intent = new Intent(mActivity, ActivityPedido.class);
            intent.putExtra(ActivityPedido.EXTRA_ITENS, oferta);
            intent.putExtra(ActivityPedido.EXTRA_SUPERMERCADO, this.supermercado);
            startActivity(intent);

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(OfertaProduto ofertaProduto) {
    }

    @Override
    public void updateAdapter() {
        adapter.notifyDataSetChanged();
        if (adapter.getData() != null && !adapter.getData().isEmpty()){
            if (adapter != null && adapter.getData() != null && !adapter.getData().isEmpty()) {
                for (OfertaProduto ofertaProduto : adapter.getData()) {
                    if (ofertaProduto.isSelected()) {
                        menu.findItem(R.id.menu_produto_carrinho).setVisible(true);
                        break;
                    }
                }

            }
        }
    }
}
