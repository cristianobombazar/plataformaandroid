package com.br.esupermarketapp.webservice;

import com.br.esupermarketapp.util.Task;

public interface IntegrationCallBack {

    void publishCallBack(PostExecute postExecute);
    void executeTask(Task task);
    void beforeUpdateAdapter();
    void afterUpdateAdapter();
}
