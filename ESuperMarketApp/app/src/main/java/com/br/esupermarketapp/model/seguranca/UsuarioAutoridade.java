package com.br.esupermarketapp.model.seguranca;

@SuppressWarnings("serial")
public class UsuarioAutoridade {
	
	private Integer id;
	private Usuario usuario;
	private String authority;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getAuthority() {
		return authority;
	}

}
