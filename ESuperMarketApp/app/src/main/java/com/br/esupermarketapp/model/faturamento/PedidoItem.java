package com.br.esupermarketapp.model.faturamento;

import com.br.esupermarketapp.model.cadastro.Produto;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class PedidoItem implements Serializable {

	private Integer id;
	private Produto produto;
    @JsonBackReference
	private Pedido pedido;
	private BigDecimal valor;
	private BigDecimal quantidade;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public Pedido getPedido() {
		return pedido;
	}
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public BigDecimal getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}
	
	public BigDecimal getValorTotal() {
		return quantidade.multiply(valor);
	}
	
}
