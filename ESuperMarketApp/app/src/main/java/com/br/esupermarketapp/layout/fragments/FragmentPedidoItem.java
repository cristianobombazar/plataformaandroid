package com.br.esupermarketapp.layout.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.Toast;
import com.br.esupermarketapp.R;
import com.br.esupermarketapp.layout.adapters.AdapterPedidoItem;
import com.br.esupermarketapp.model.faturamento.Pedido;
import com.br.esupermarketapp.model.faturamento.PedidoItem;
import java.util.List;

public class FragmentPedidoItem extends Fragment implements AdapterPedidoItem.ItemClickListener {

    public static final String TITLE = "Produtos";

    private AdapterPedidoItem adapter;
    private View root;
    private RecyclerView recyclerView;
    private EditText etQuantidade;

    private Pedido pedido;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root == null){
            root = inflater.inflate(R.layout.fragment_pedido_item, container, false);
            recyclerView = (RecyclerView) root.findViewById(R.id.fragment_pedido_item_recyler);
            etQuantidade = (EditText) root.findViewById(R.id.pedido_item_quantidade);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            updateAdapterInicial();
        }
        return root;
    }

    @Override
    public void onItemClick(final PedidoItem pedidoItem) {
    }

    @Override
    public void setOnLongClickListener(final PedidoItem pedidoItem) {
        final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
        b.setMessage("Deseja exculuir o produto "+pedidoItem.getProduto().getDescricao()+" dos produtos?");
        b.setPositiveButton("Cancelar", null);
        b.setNegativeButton("Excluir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                adapter.removerProduto(pedidoItem);
                updateAdapter();
                Toast.makeText(getActivity(), "Produto removido", Toast.LENGTH_SHORT).show();
            }
        });
        b.show();
    }

    @Override
    public void updateAdapter(){
        adapter.notifyDataSetChanged();
    }

    public void updateAdapterInicial(){
        adapter = new AdapterPedidoItem(pedido, getActivity().getApplicationContext(), this);
        LayoutAnimationController layoutAnimationController = AnimationUtils.loadLayoutAnimation(getActivity().getApplicationContext(), R.anim.layout_animation_slide_right);
        recyclerView.setLayoutAnimation(layoutAnimationController);
        recyclerView.setAdapter(adapter);
        recyclerView.scheduleLayoutAnimation();
    }

    public static FragmentPedidoItem newInstace(Pedido pedido){
        FragmentPedidoItem fragment = new FragmentPedidoItem();
        fragment.pedido = pedido;
        return fragment;
    }

    public List<PedidoItem> fillPedidoItens(){
        return adapter.getData();
    }

}
