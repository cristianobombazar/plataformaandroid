package com.br.esupermarketapp.gui;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.NumberKeyListener;
import android.util.AttributeSet;

public class CepEditText extends AppCompatEditText {

    private int						positioning[]		= { 0, 1, 2, 3, 4, 5, 7, 8, 9 };
    private boolean					isUpdating;
    private final KeylistenerNumber	keylistenerNumber	= new KeylistenerNumber();

    public CepEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize();
    }

    public CepEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public CepEditText(Context context) {
        super(context);
        initialize();
    }

    public String getCleanText() {
        String text = this.getText().toString();
        text.replaceAll("[^0-9]*", "");
        return text;
    }

    private void initialize() {
        final int maxNumberLength = 8;
        this.setKeyListener(keylistenerNumber);

        this.setText("     -   ");
        this.setSelection(1);

        this.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String current = s.toString();
                if (isUpdating) {
                    isUpdating = false;
                    return;
                }

                String number = current.replaceAll("[^0-9]*", "");
                if (number.length() > maxNumberLength)
                    number = number.substring(0, maxNumberLength);
                int length = number.length();

                String paddedNumber = String.format("%1$-" + maxNumberLength + "s", number);

                String part1 = paddedNumber.substring(0, 5);
                String part2 = paddedNumber.substring(5, 8);

                String cep = part1 + "-" + part2;

                isUpdating = true;
                CepEditText.this.setText(cep);
                CepEditText.this.setSelection(positioning[length]);

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }

    private class KeylistenerNumber extends NumberKeyListener {
        public int getInputType() {
            return InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;
        }

        @Override
        protected char[] getAcceptedChars() {
            return new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        }
    }
}