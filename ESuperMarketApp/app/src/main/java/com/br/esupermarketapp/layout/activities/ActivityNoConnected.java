package com.br.esupermarketapp.layout.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.br.esupermarketapp.R;
import com.br.esupermarketapp.util.Util;

/**
 * Created by crist on 27/11/2017.
 */

public class ActivityNoConnected extends AppCompatActivity implements View.OnClickListener {

    private Button btTentarNovamente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_connection);

        btTentarNovamente = (Button) findViewById(R.id.botao_sem_internet);
        btTentarNovamente.setOnClickListener(this);
        Snackbar.make(btTentarNovamente, "Sem conexão com a internet", Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    @Override
    public void onClick(View view) {
        if (view == btTentarNovamente){
            if (Util.verificaConexao(this)){
                startActivity(new Intent(this, ActivityMain.class));
            }else{
                Snackbar.make(btTentarNovamente, "Sem conexão com a internet", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
