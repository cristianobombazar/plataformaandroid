package com.br.esupermarketapp.model.cadastro;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cliente implements Serializable {
	
	public static final Integer CLIENTE_TIPO_FISICA = 1;
	public static final Integer CLIENTE_TIPO_JURIDICA = 2;
	
	public static final Integer INDICADOR_USO_ATIVO = 1;
	public static final Integer INDICADOR_USO_INATIVO = 2;
	
	private Integer id;
	private Supermercado supermercado;
	private String nome;
	private Integer tipo;
	private String cpfCnpj;
	private String rg;
	private String telefone;
	private String email;
	private Integer indicadorUso;
	private ClienteEndereco endereco;
	private List<ClienteCartao> cartoes = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getRg() {
		return rg;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public ClienteEndereco getEndereco() {
		return endereco;
	}
	public void setEndereco(ClienteEndereco endereco) {
		this.endereco = endereco;
	}
	public List<ClienteCartao> getCartoes() {
		return Collections.unmodifiableList(cartoes);
	}
	public void adicionarCartao(ClienteCartao cartao) {
		this.cartoes.add(cartao);
	}
	public void removerCartao(ClienteCartao cartao) {
		this.cartoes.remove(cartao);
	}
	public void setIndicadorUso(Integer indicadorUso) {
		this.indicadorUso = indicadorUso;
	}
	public Integer getIndicadorUso() {
		return indicadorUso;
	}
	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}
	public Supermercado getSupermercado() {
		return supermercado;
	}
	
}
