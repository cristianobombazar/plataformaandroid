package com.br.esupermarketapp.layout.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.br.esupermarketapp.R;
import com.br.esupermarketapp.util.Util;

public class ActivitySplash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final Activity activity = this;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Util.verificaConexao(activity)){
                    startActivity(new Intent(ActivitySplash.this, ActivityMain.class));
                }else{
                    startActivity(new Intent(ActivitySplash.this, ActivityNoConnected.class));
                }
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        }, 1500);
    }
}
