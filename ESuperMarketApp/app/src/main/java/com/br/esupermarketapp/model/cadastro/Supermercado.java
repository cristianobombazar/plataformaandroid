package com.br.esupermarketapp.model.cadastro;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class Supermercado implements Serializable {

	public static final Integer INDICADOR_USO_ATIVO = 1;
	public static final Integer INDICADOR_USO_INATIVO = 2;

	private Integer id;
	private String razaoSocial;
	private String cnpj;
	private Integer indicadorUso;
	private SupermercadoEndereco endereco;

	private String logo;

	public void preSave() {
		this.getEndereco().setSupermercado(this);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Integer getIndicadorUso() {
		return indicadorUso;
	}

	public void setIndicadorUso(Integer indicadorUso) {
		this.indicadorUso = indicadorUso;
	}

	public SupermercadoEndereco getEndereco() {
		return endereco;
	}

	public void setEndereco(SupermercadoEndereco endereco) {
		this.endereco = endereco;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getLogo() {
		return logo;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Supermercado)) return false;
		Supermercado that = (Supermercado) o;
		return id.equals(that.id);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

}
