package com.br.esupermarketapp;


import android.support.v4.app.Fragment;

import com.br.esupermarketapp.layout.fragments.FragmentProduto;
import com.br.esupermarketapp.model.cadastro.Supermercado;
import com.br.esupermarketapp.model.faturamento.Pedido;

public class Global {

    public static Pedido pedido;
    public static Supermercado supermercado;

    public static Fragment previusFragment;

    public static void setPreviusFragment(Fragment previusFragment) {
        Global.previusFragment = previusFragment;
    }
}
