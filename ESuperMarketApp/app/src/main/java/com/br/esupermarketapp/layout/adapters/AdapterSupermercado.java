package com.br.esupermarketapp.layout.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.br.esupermarketapp.R;
import com.br.esupermarketapp.model.cadastro.Supermercado;
import com.br.esupermarketapp.model.cadastro.SupermercadoEndereco;

import java.util.Collections;
import java.util.List;


public class AdapterSupermercado extends RecyclerView.Adapter<AdapterSupermercado.SupermercadoViewHolder> {

    private Context context;
    private List<Supermercado> supermercados;
    private static ItemClickListener itemClickListener;

    public AdapterSupermercado(List<Supermercado> supermercados, Context context, ItemClickListener itemClickListener){
        this.supermercados = supermercados;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public SupermercadoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_supermercado, parent, false);
        return new SupermercadoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SupermercadoViewHolder holder, int position) {
        Supermercado supermercado = supermercados.get(position);
        holder.supermercado = supermercado;
        holder.header.setText(supermercado.getRazaoSocial());
        SupermercadoEndereco endereco = supermercado.getEndereco();
        holder.content.setText(endereco.getRua()+", "+endereco.getBairro().getNome()+" - "+endereco.getMunicipio().getNome()+", "+endereco.getCep().getCodigo());
    }

    @Override
    public int getItemCount() {
        return supermercados.size();
    }

    public List<Supermercado> getData(){
        return Collections.unmodifiableList(supermercados);
    }

    public void addSupermercado(Supermercado supermercado){
        this.supermercados.add(0, supermercado);
    }

    public void removeSupermercado(Supermercado supermercado){
        this.supermercados.remove(supermercado);
    }

    public static class SupermercadoViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView  header;
        private TextView  content;
        private Supermercado supermercado;

        public SupermercadoViewHolder(View itemView){
            super(itemView);
            header  = (TextView) itemView.findViewById(R.id.item_supermercado_header);
            content = (TextView) itemView.findViewById(R.id.item_supermercado_content);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View itemView) {
            itemClickListener.onItemClick(supermercado);
        }
    }

    public interface ItemClickListener{
        void onItemClick(Supermercado supermercado);
    }
}
