package com.br.esupermarketapp.util;

public enum TaskType {

    LISTA_SUPERMERCADO, LISTA_PRODUTOS, SALVAR_PEDIDO, BUSCA_PRODUTO_QRCODE, LISTA_FORMA_PAGAMENTO

}
