package com.br.esupermarketapp.layout.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.br.esupermarketapp.R;
import com.br.esupermarketapp.model.cadastro.Cliente;
import com.br.esupermarketapp.model.cadastro.ClienteCartao;
import com.br.esupermarketapp.model.faturamento.Pedido;
import com.br.esupermarketapp.util.Util;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;


public class FragmentPedidoCartao extends Fragment {

    public static final String TITLE = "Cartão";

    private EditText etNumeroCartao;
    private EditText etNomeImpresso;
    private EditText etValidadeCartao;
    private EditText etCodigoSeguranca;
    private Spinner  spinnerTipo;
    private View root;

    private List<Integer> tiposCartao = Arrays.asList(1,2,3,4);

    private Pedido pedido;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root == null){
            root = inflater.inflate(R.layout.fragment_pedido_cartao, container, false);
            etNumeroCartao    = (EditText) root.findViewById(R.id.pedido_cartao_numero);
            etNomeImpresso    = (EditText) root.findViewById(R.id.pedido_cartao_nome);
            etValidadeCartao  = (EditText) root.findViewById(R.id.pedido_cartao_data_validade);
            etCodigoSeguranca = (EditText) root.findViewById(R.id.pedido_cartao_codigo_segurança);
            spinnerTipo       = (Spinner)  root.findViewById(R.id.pedido_cartao_tipo);
            updateAdapter();
            fillFields();
        }
        return root;
    }

    public static FragmentPedidoCartao newInstace(Pedido pedido){
        FragmentPedidoCartao fragment = new FragmentPedidoCartao();
        fragment.pedido = pedido;
        return fragment;
    }

    private void updateAdapter(){
        List<String> descricaoTipo = Arrays.asList("DÉBITO", "CRÉDITO", "ALIMENTAÇÃO", "OUTRO");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, descricaoTipo);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipo.setAdapter(adapter);
    }

    public ClienteCartao fillCartaoCliente(Cliente cliente){
        if (root == null){
            return null;
        }
        ClienteCartao clienteCartao = new ClienteCartao();
        clienteCartao.setId(null);
        clienteCartao.setTipo(spinnerTipo.getSelectedItemPosition());
        clienteCartao.setBandeira(1);
        clienteCartao.setCliente(cliente);
        clienteCartao.setCodigoSeguranca(Integer.parseInt(etCodigoSeguranca.getText().toString()));
        clienteCartao.setNomeImpresso(etNomeImpresso.getText().toString());
        clienteCartao.setNumeroCartao(etNumeroCartao.getText().toString());
        try {
            clienteCartao.setDataExpiracao(Util.toDate(etValidadeCartao.getText().toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return clienteCartao;
    }

    private void fillFields(){
        etNumeroCartao.setText("4556046893748288");
        etNomeImpresso.setText("EDUARDO YUCHO");
        etCodigoSeguranca.setText("019");
        etValidadeCartao.setText("11/2020");
    }
}
