package com.br.esupermarketapp.model.faturamento;

import com.br.esupermarketapp.model.cadastro.Cliente;
import com.br.esupermarketapp.model.cadastro.FormaPagamento;
import com.br.esupermarketapp.model.cadastro.Supermercado;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pedido implements Serializable {
	
	public static final int STATUS_PEDIDO_PENDENTE = 1;
	public static final int STATUS_PEDIDO_LIBERADO = 2;
	public static final int STATUS_PEDIDO_FATURADO = 3;
	public static final int STATUS_PEDIDO_CANCELADO = 4;
	
	private Integer id;
	private Cliente cliente;
	private Supermercado supermercado;
	private FormaPagamento formaPagamento;
	private Oferta oferta;
	private Integer situacao;
	private Date data;
	private String obs;
	private List<PedidoItem> itens = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public Supermercado getSupermercado() {
		return supermercado;
	}

	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}
	
	public List<PedidoItem> getItens(){
		return itens;
	}

	public void setItens(List<PedidoItem> itens) {
		this.itens = itens;
	}

	public void adicionaItem(PedidoItem item) {
		itens.add(item);
	}
	
	public void removeItem(PedidoItem item) {
		itens.remove(item);
	}
	
	public Integer getQuantidadeItens() {
		return itens.size();
	}
	
	public void setOferta(Oferta oferta) {
		this.oferta = oferta;
	}
	
	public Oferta getOferta() {
		return oferta;
	}
}
