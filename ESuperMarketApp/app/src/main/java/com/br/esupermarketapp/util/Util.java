package com.br.esupermarketapp.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.SimpleTimeZone;

/**
 * Created by crist on 09/11/2017.
 */

public class Util {

    public static String formatarMoedaReal(double valor) {
        return formatarMoedaReal(new BigDecimal(valor));
    }

    public static String formatarMoedaReal(BigDecimal valor) {
        if (valor != null && !valor.equals("")) {
            return new DecimalFormat("###,###,##0.00", new DecimalFormatSymbols(new Locale("pt", "BR"))).format(valor);
        }
        return null;
    }

    public static String formatarValor(double valor) {
        return formatarValor(new BigDecimal(valor));
    }

    public static String formatarValor(BigDecimal valor) {
        if (valor != null){
            return new DecimalFormat("###,###,##0.00").format(valor);
        }
        return null;
    }

    public static String formatDate(Date date){
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    public static String removerCaracteres(String text){
        return text.replaceAll("[^0-9]", "");
    }

    public static Date toDate(String date) throws ParseException {
       return  new SimpleDateFormat("MM/yyyy").parse(date);
    }

    public static boolean verificaConexao(Activity activity) {
        ConnectivityManager conectivtyManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conectivtyManager.getActiveNetworkInfo() != null&& conectivtyManager.getActiveNetworkInfo().isAvailable() && conectivtyManager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
