package com.br.esupermarketapp.webservice;

import android.os.AsyncTask;
import com.br.esupermarketapp.model.cadastro.FormaPagamento;
import com.br.esupermarketapp.model.cadastro.Supermercado;
import com.br.esupermarketapp.model.faturamento.Oferta;
import com.br.esupermarketapp.model.faturamento.Pedido;
import com.br.esupermarketapp.model.seguranca.Usuario;
import com.br.esupermarketapp.util.TaskType;
import com.br.esupermarketapp.util.Task;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IntegrationTaskService extends AsyncTask<Task, Void, PostExecute> {

    private IntegrationCallBack callBack;
    public static final String URI = "http://192.168.0.105:9090/sys/";

    public static final String USER_ACESS     = "sys";
    public static final String PASSWORD_ACESS = "j147f963x";

    public static RestTemplate restTemplate = null;
    public static MappingJackson2HttpMessageConverter jacksonConverter = null;

    public IntegrationTaskService(IntegrationCallBack callBack){
        this.callBack = callBack;
    }

    @Override
    protected PostExecute doInBackground(Task... params) {
        Task task = params[0];
        if (task.getType() == null){
            throw new RuntimeException("Define pelo menos um tipode execução.");
        }
        try {
            if (restTemplate == null){
                login();
            }
            if (task.getType().equals(TaskType.LISTA_SUPERMERCADO)){
                return new PostExecute(findAllSupermercado(), task);
            }else if (task.getType().equals(TaskType.LISTA_PRODUTOS)){
                if (!task.getParameters().isEmpty()){
                    if (task.getParameters().get(0) instanceof  Supermercado){
                        Supermercado supermercado = (Supermercado) task.getParameters().get(0);
                        return new PostExecute(findOferta(supermercado), task);
                    }
                }
                return null;
            }else if (task.getType().equals(TaskType.SALVAR_PEDIDO)){
                if (!task.getParameters().isEmpty()){
                    if (task.getParameters().get(0) instanceof Pedido){
                        Pedido pedido = (Pedido) task.getParameters().get(0);
                        return new PostExecute(sendPedido(pedido), task);
                    }
                }
            }else if(task.getType().equals(TaskType.LISTA_FORMA_PAGAMENTO)) {
                if (!task.getParameters().isEmpty()) {
                    if (task.getParameters().get(0) instanceof Supermercado) {
                        Supermercado supermercado = (Supermercado) task.getParameters().get(0);
                        return new PostExecute(findAllFormaPagamento(supermercado), task);
                    }
                }
            }else if (task.getType().equals(TaskType.BUSCA_PRODUTO_QRCODE)){
                if (!task.getParameters().isEmpty()) {
                    Long qrCodeValue = (Long) task.getParameters().get(0);
                    return new PostExecute(findOferaByQrCode(qrCodeValue), task);
                }
            }else{
                throw new Exception("Tipo de integração não implementada");
            }
        } catch (Exception e) {
            return new PostExecute(null, task);
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {

    }

    @Override
    protected void onPostExecute(PostExecute postExecute) {
        callBack.publishCallBack(postExecute);
    }

    public Oferta findOferta(Supermercado supermercado) throws Exception{
        return buscaOferta(getRestTemplate(), supermercado);
    }

    public List<Supermercado> findAllSupermercado() throws Exception{
        return buscaSupermercado(getRestTemplate());
    }

    public List<FormaPagamento> findAllFormaPagamento(Supermercado supermercado) throws Exception{
        return buscaFormaPagamento(getRestTemplate(), supermercado);
    }

    public boolean sendPedido(Pedido pedido) throws Exception{
        return sendPedido(pedido, getRestTemplate());
    }

    public Oferta findOferaByQrCode(Long qrCode) throws Exception{
        return findOferaByQrCode(qrCode, getRestTemplate());
    }

    private void login() throws Exception {
        restTemplate = getRestTemplate();
        ResponseEntity<Object> response = restTemplate.postForEntity(IntegrationTaskService.URI + "seguranca/login", new Usuario(USER_ACESS, PASSWORD_ACESS), Object.class);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new RuntimeException("Falha na autenticação\n" + response.getStatusCode().value() + " " + response.getStatusCode().getReasonPhrase());
        }
        String auth = response.getHeaders().get("Authorization").get(0);
        restTemplate.setInterceptors(new ArrayList<ClientHttpRequestInterceptor>());
        restTemplate.getInterceptors().add(new AuthenticationHeaderInterceptor(auth));
    }

    private List<Supermercado> buscaSupermercado(RestTemplate restTemplate)  throws Exception{
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(IntegrationTaskService.URI + "cadastro/supermercado/");
        String uri = builder.build().toUriString();
        Supermercado[] supermercados = restTemplate.getForObject(uri, Supermercado[].class);
        return new ArrayList<>(Arrays.asList(supermercados));
    }

    private Oferta buscaOferta(RestTemplate restTemplate, Supermercado supermercado)  throws Exception{
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(IntegrationTaskService.URI + "faturamento/oferta/findOferta");
        builder.queryParam("supermercadoID", supermercado.getId());
        String uri = builder.build().toUriString();
        return restTemplate.getForObject(uri, Oferta.class);
    }

    private List<FormaPagamento> buscaFormaPagamento(RestTemplate restTemplate, Supermercado supermercado) throws Exception{
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(IntegrationTaskService.URI + "cadastro/formaPagamento/findAllBySupermercado");
        builder.queryParam("supermercadoID", supermercado.getId());
        String uri = builder.build().toUriString();
        FormaPagamento[] formasPagamento = restTemplate.getForObject(uri, FormaPagamento[].class);
        return new ArrayList<>(Arrays.asList(formasPagamento));
    }

    private boolean sendPedido(Pedido pedido, RestTemplate restTemplate) throws Exception{
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(IntegrationTaskService.URI + "faturamento/pedido/forMobile");
        String uri = builder.build().toUriString();
        ResponseEntity<Pedido> responseEntity = restTemplate.postForEntity(uri, pedido, Pedido.class);
        if (responseEntity.getStatusCode().equals(HttpStatus.OK)){
            return true;
        }else{
            return false;
        }
    }

    private Oferta findOferaByQrCode(Long qrCode, RestTemplate restTemplate) throws  Exception{
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(IntegrationTaskService.URI + "faturamento/oferta/findOfertaByQrCode");
        builder.queryParam("qrCode", qrCode);
        String uri = builder.build().toUriString();
        return restTemplate.getForObject(uri, Oferta.class);
    }

    private static RestTemplate getRestTemplate()  throws Exception {
        if (restTemplate == null){
            createRestTemplate();
        }
        return restTemplate;
    }

    private static void createRestTemplate() throws Exception{
        restTemplate = new RestTemplate();
        jacksonConverter = new MappingJackson2HttpMessageConverter();
        jacksonConverter.getObjectMapper().setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE).configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        restTemplate.getMessageConverters().add(jacksonConverter);
    }



}
