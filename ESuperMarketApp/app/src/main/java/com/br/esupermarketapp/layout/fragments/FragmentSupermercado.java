package com.br.esupermarketapp.layout.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;

import com.br.esupermarketapp.Global;
import com.br.esupermarketapp.layout.activities.ActivityMain;
import com.br.esupermarketapp.layout.adapters.AdapterSupermercado;
import com.br.esupermarketapp.model.cadastro.Supermercado;
import com.br.esupermarketapp.util.TaskType;
import com.br.esupermarketapp.util.Task;
import com.br.esupermarketapp.webservice.IntegrationCallBack;
import com.br.esupermarketapp.webservice.IntegrationTaskService;
import com.br.esupermarketapp.webservice.PostExecute;

import java.util.List;
import com.br.esupermarketapp.R;

public class FragmentSupermercado extends Fragment implements AdapterSupermercado.ItemClickListener, IntegrationCallBack {

    private View                root        = null;
    private ProgressBar         progress    = null;
    private RecyclerView        recyclerView = null;
    private AdapterSupermercado adapter     = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root == null){
            root = inflater.inflate(R.layout.fragment_supermercado, container, false);
            progress = (ProgressBar) root.findViewById(R.id.frag_supemercado_progress);
            recyclerView = (RecyclerView) root.findViewById(R.id.fragment_supermercado_recyler);
            progress.setIndeterminate(true);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            ((ActivityMain) getActivity()).setTitleActionBar("Supermercados");
        }
        return root;
    }


    @Override
    public void onStart() {
        super.onStart();
        Task task = new Task(TaskType.LISTA_SUPERMERCADO);
        executeTask(task);
    }

    @Override
    public void onItemClick(Supermercado supermercado) {
        Bundle parameters = new Bundle();
        parameters.putSerializable(FragmentProduto.EXTRA_SUPERMERCADO, supermercado);
        FragmentProduto fragmentProduto = new FragmentProduto();
        fragmentProduto.setArguments(parameters);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, fragmentProduto);
        transaction.commit();
        Global.setPreviusFragment(fragmentProduto);
    }

    @Override
    public void executeTask(Task task) {
        beforeUpdateAdapter();
        new IntegrationTaskService(this).execute(task);
    }

    @Override
    public void publishCallBack(PostExecute postExecute) {
        if (postExecute.getTaskExecuted().getType().equals(TaskType.LISTA_SUPERMERCADO)){
            updateAdapter((List<Supermercado>) postExecute.getObjectReturnedFromIntegrationTask());
            afterUpdateAdapter();
        }
    }

    @Override
    public void beforeUpdateAdapter() {
        progress.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

    }

    @Override
    public void afterUpdateAdapter() {
        progress.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void updateAdapter(List<Supermercado> supermercados){
        if (supermercados != null && !supermercados.isEmpty()){
            adapter = new AdapterSupermercado(supermercados, getActivity().getApplicationContext(), this);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

            LayoutAnimationController layoutAnimationController = AnimationUtils.loadLayoutAnimation(getActivity().getApplicationContext(), R.anim.layout_animation_slide_right);
            recyclerView.setLayoutAnimation(layoutAnimationController);

            recyclerView.setAdapter(adapter);
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
